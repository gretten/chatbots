<?php

/**
 * Контроллер для авторизации
 * Class AuthController
 */
class AuthController extends ControllerBase
{

    /**
     * @api {get} auth/ping ping
     * @apiName ping
     * @apiGroup Auth
     * @apiPermission Все пользователи
     * @apiDescription Получает информацию по текущему пользователю
     * @apiSuccess {json} session_id Идентификатор сессии
     * @apiSuccess {json} user_id ИД пользователя
     */
    public function pingAction()
    {
        $response = $this->prepareResponse();
        $data = array(
            'user_id' => 0,
            'session_id' => 0,
        );
        if ($this->session->has('uid') && $this->session->get('uid')) {
            $data['user_id'] = $this->session->get('uid');
            $data['session_id'] = $this->session->getId();
        }
        $response->setJsonContent($data);
        return $response;
    }


    /**
     * @api {post} auth/credentials credentials
     * @apiName credentials
     * @apiGroup Auth
     * @apiPermission Неавторизованные пользователи
     * @apiDescription Осуществляет авторизацию через пару email-пароль
     * @apiParam {string} email Мэил пользователя
     * @apiParam {string} password Пароль пользователя
     * @apiSuccess {json} token Токен доступа
     */
    public function credentialsAction()
    {
        $result_data = array();

        $response = $this->prepareResponse();
        if ($this->request->get('email') && $this->request->get('password')) {
            $params = array(
                'email' => $this->request->get('email'),
                'pass' => Users::encryptUserPassword($this->request->get('password'))
            );

            $user = Users::findFirst(array(
                'conditions' => 'email = :email: AND pass = :pass:',
                'bind' => $params
            ));

            if ($user && isset($user->id)) {
                $new_token = Tokens::createNewTokenForUser($user->id);
                $this->session->set('uid', $user->id);
                $result_data = array("token" => $new_token->token);
            } else {
                $result_data = array('error' => 'GENERAL.ERRORS.WRONG_CREDENTIALS');
            }
        }
        $response->setJsonContent($result_data);
        return $response;
    }

    /**
     * @api {post} auth/register register
     * @apiName register
     * @apiGroup Auth
     * @apiPermission Неавторизованные пользователи
     * @apiDescription Регистрация пользователя
     * @apiParam {string} email емеил пользователя
     * @apiParam {string} password пароль пользователя
     * @apiParam {string} name Optional имя пользователя
     * @apiParam {string} lastname Optional фамилия пользователя
     * @apiParam {string} phone Optional телефон пользователя
     * @apiSuccess {json} user объект пользователя с присвоенным ид и без пароля
     */
    public function registerAction()
    {
        $response = $this->prepareResponse();
        //TODO сделать отдельную проверку на существующий email
        if ($this->request->get('email') && $this->request->get('password')) {
            $user = new Users();
            $user->id = '';
            $user->name = $this->request->get('name') ? $this->request->get('name') : '';
            $user->lastname = $this->request->get('lastname') ? $this->request->get('lastname') : '';
            $user->phone = $this->request->get('phone') ? $this->request->get('phone') : '';
            $user->email = $this->request->get('email');
            $user->role_id = 1;
            $user->end_date = time() + 366660;
            $user->created = time();
            $user->pass = Users::encryptUserPassword($this->request->get('password'));
            $success = $user->create();
            if ($success) {
                unset($user->pass);
                $response->setJsonContent($user);
            } else {
                $response->setJsonContent(array('error' => 'GENERAL.ERRORS.CANT_CREATE_USER'));
            }
        } else {
            $response->setJsonContent(array('error' => 'GENERAL.ERRORS.MISSING_EMAIL_OR_PASSWORD'));
        }

        return $response;
    }


    /**
     * @api {get} auth/logout logout
     * @apiUse SecurityCheck
     * @apiName logout
     * @apiGroup Auth
     * @apiPermission Авторизованные пользователи
     * @apiDescription Осуществляет выход из приложения
     */
    public function logoutAction()
    {
        $this->session->destroy();
        $this->response->redirect('auth/ping');
    }

    /**
     * @api {post} auth/remindPassword remind_password
     * @apiName remindPassword
     * @apiGroup Auth
     * @apiPermission Неавторизованные пользователи
     * @apiDescription Отправка письма с кодом для востановления пароля пользователю на почту (но пока почты нет, просто плюет обратно созданный обьект с хешем), все предидущие хеши при этом стираются
     * @apiParam {string} email Мейл пользователя, которому отправлять это добро
     * @apiSuccess {json} response "response->success" в случае успеха (но пока почты нет, просто плюет обратно созданный обьект с хешем)
     */
    public function remindPasswordAction()
    {
        $result_data = array();

        $response = $this->prepareResponse();
        if ($this->request->get('email')) {
            $params = array(
                'email' => $this->request->get('email')
            );

            $user = Users::findFirst(array(
                'conditions' => 'email = :email:',
                'bind' => $params
            ));

            if ($user && isset($user->id)) {
                $new_hash = UserRemindPassword::createNewHashForUser($user->id);
                if ($new_hash) {
                    $mailer = Phalcon\DI::getDefault()->getShared('mailer');
                    $message_text = "Перейдите по ссылке для воостановления пароля <a href='".$this->config->application->projectUrl."restore-password/".$new_hash->check_hash."'>".$this->config->application->projectUrl."restore-password/".$new_hash->check_hash."</a>";
                    $message = $mailer->createMessage()
                        ->to($user->email, $user->email)
                        ->subject('Восстановление пароля')
                        ->content($message_text);

                    $success = $message->send();
                    if(!$success){
                        $result_data = array('error' => 'GENERAL.ERRORS.CANT_SEND_MAIL');
                    }else{
                        $result_data = $new_hash;
                    }
                    //$result_data = array('response' => 'success');

                } else {
                    $result_data = array('error' => 'GENERAL.ERRORS.CANT_CREATE_NEW_HASH');
                }

            } else {
                $result_data = array('error' => 'GENERAL.ERRORS.WRONG_EMAIL');
            }
        } else {
            $result_data = array('error' => 'GENERAL.ERRORS.MISSING_EMAIL');
        }
        $response->setJsonContent($result_data);
        return $response;
    }

    /**
     * @api {post} auth/recoverAndChangePassword recover_and_change_password
     * @apiName recoverAndChangePassword
     * @apiGroup Auth
     * @apiPermission Неавторизованные пользователи
     * @apiDescription Изменение пароля пользователя + автоматический логин в систему
     * @apiParam {string} hash Хеш пользователя для восстановления пароля
     * @apiParam {string} password новый пароль пользователя
     * @apiSuccess {json} token токен пользователя
     */
    public function recoverAndChangePasswordAction()
    {
        $result_data = array();

        $response = $this->prepareResponse();
        if ($this->request->get('hash') && $this->request->get('password')) {
            $params = array(
                'check_hash' => $this->request->get('hash')
            );

            $user_remind_password = UserRemindPassword::findFirst(array(
                'conditions' => 'check_hash = :check_hash:',
                'bind' => $params
            ));

            if ($user_remind_password && isset($user_remind_password->id)) {
                if ($user_remind_password->expiration_date > time()) {
                    $data_array = array();
                    $data_array['id'] = $user_remind_password->uid;
                    $data_array['password'] = Users::encryptUserPassword($this->request->get('password'));
                    if ($result = Users::updateUserPassword($data_array)) {
                        //если мы успешно поменяли пользователю, пытаемся выдать токен
                        $new_token = Tokens::createNewTokenForUser($result->id);
                        $this->session->set('uid', $result->id);
                        $result_data = array("token" => $new_token->token);
                    }else {
                        $result_data = array('error' => 'GENERAL.ERRORS.CANT_CREATE_NEW_PASSWORD');
                    }
                } else {
                    $result_data = array('error' => 'GENERAL.ERRORS.HASH_EXPIRED');
                }
            } else {
                $result_data = array('error' => 'GENERAL.ERRORS.WRONG_HASH');
            }
        } else {
            $result_data = array('error' => 'GENERAL.ERRORS.HASH_OR_PASSWORD_MISSING');
        }
        $response->setJsonContent($result_data);
        return $response;
    }
}

