<?php

/**
 * Контроллер для работы с тоннелями
 * Class AuthController
 */
class DispatchController extends ControllerBase
{
    /**
     * @api {get} dispatch/ getUserDispatches
     * @apiName getUserDispatches
     * @apiGroup Dispatch
     * @apiPermission Аутентифицированные
     * @apiDescription Получает информацию о рассылках текущего пользователя
     * @apiParam {string} uid User id
     * @apiUse SecurityCheck
     * @apiSuccess {json} result Массив ид-> обьект рассылки
     */
    public function indexAction()
    {
        $response = $this->prepareResponse();
        $result_array = array();
        $uid = $this->session->get('uid');
        $result_array['result'] = Dispatch::getUserDispatches($uid);
        $response->setJsonContent($result_array);
        return $response;
    }

    /**
     * @api {post} dispatch/addDispatch addDispatch
     * @apiName addDispatch
     * @apiGroup Dispatch
     * @apiPermission Аутентифицированные
     * @apiDescription добавляет новую рассылку
     * @apiParam {json} request Объект запроса...
     * @apiUse SecurityCheck
     * @apiSuccess {json} result Массив ид-> обьект рассылки
     */
    public function addDispatchAction()
    {
        $response = $this->prepareResponse();
        if (!$this->request->get('request')) {
            $response->setJsonContent(array('error' => 'GENERAL.ERRORS.MISSING_PARAM'));
            return $response;
        }
        $request_array = json_decode($this->request->get('request'));

        if (!$request_array['bot_vk_id'] &&
            !$request_array['bot_fb_id'] &&
            !$request_array['bot_telegram_id'] &&
            !$request_array['bot_skype_id'] &&
            !$request_array['bot_viber_id']) {
            $response->setJsonContent(array('error' => 'GENERAL.ERRORS.MISSING_BOT'));
            return $response;
        }
        if (!$request_array['message']) {
            $response->setJsonContent(array('error' => 'GENERAL.ERRORS.MISSING_MESSAGE'));
            return $response;
        }

        $uid = $this->session->get('uid');
        $data_array = array();

        $data_array['id'] = '';
        $data_array['uid'] = $uid;

        self::_dispatchFormDataArrayFromRequest($data_array, $request_array);

        $array_to_check = array(
            array("bot_id" => $data_array['bot_vk_id'], "bot_enum_id" => VK_ID),
            array("bot_id" => $data_array['bot_fb_id'], "bot_enum_id" => FB_ID),
            array("bot_id" => $data_array['bot_telegram_id'], "bot_enum_id" => TELEGRAM_ID),
            array("bot_id" => $data_array['bot_skype_id'], "bot_enum_id" => SKYPE_ID),
            array("bot_id" => $data_array['bot_viber_id'], "bot_enum_id" => VIBER_ID),

        );
        if (!BotsController::checkUserAccessToBotsByBotIdBatch($array_to_check, $uid)) {
            $response->setJsonContent(array('error' => 'GENERAL.ERRORS.ACCES_DENIED'));
            return $response;
        }


        $result = self::_dispatchAddDispatch($uid, json_encode($request_array['message']), $data_array);
        $response->setJsonContent($result);
        return $response;
    }

    private function _dispatchFormDataArrayFromRequest(&$data_array, $request_array)
    {
        $data_array['name'] = $request_array['name'] ? $request_array['name'] : '';
        $data_array['position'] = $request_array['position'] ? $request_array['position'] : '1';
        $data_array['date'] = $request_array['date'] ? $request_array['date'] : time();
        $data_array['bot_vk_id'] = $request_array['bot_vk_id'] ? $request_array['bot_vk_id'] : '';
        $data_array['bot_fb_id'] = $request_array['bot_fb_id'] ? $request_array['bot_fb_id'] : '';
        $data_array['bot_telegram_id'] = $request_array['bot_telegram_id'] ? $request_array['bot_telegram_id'] : '';
        $data_array['bot_skype_id'] = $request_array['bot_skype_id'] ? $request_array['bot_skype_id'] : '';
        $data_array['bot_viber_id'] = $request_array['bot_viber_id'] ? $request_array['bot_viber_id'] : '';
        $data_array['filters'] = $request_array['filters'] ? $request_array['filters'] : '';
        $data_array['active'] = $request_array['active'] ? $request_array['active'] : '0';
        $data_array['sending'] = '0';
    }

    public static function _dispatchAddDispatch($uid, $message_json, $data_array)
    {
        $message = new Message();

        if (Message::checkMessageJson($message_json)) {

            $message->uid = $uid;
            $message->json = $message_json;
            $success = $message->create();
            if (!$success) {

                $result = array('error' => 'GENERAL.ERRORS.CANT_CREATE_MESSAGE');
                return $result;
            }

        } else {
            $result = array('error' => 'GENERAL.ERRORS.NOT_VALID_MESSAGE');
            return $result;
        }
        $data_array['message_id'] = $message->id;

        if ($result = Dispatch::createUserDispatch($data_array)) {

            return $result;
        }
        $result = array('error' => $data_array);
        return $result;
    }

    /**
     * @api {post} dispatch/updateDispatch updateDispatch
     * @apiName updateDispatch
     * @apiGroup Dispatch
     * @apiPermission Аутентифицированные
     * @apiDescription обновляет рассылку
     * @apiParam {json} request Объект запроса...
     * @apiUse SecurityCheck
     * @apiSuccess {json} result Массив ид-> обьект рассылки
     */
    public function updateDispatchAction()
    {
        $response = $this->prepareResponse();
        if (!$this->request->get('request')) {
            $response->setJsonContent(array('error' => 'GENERAL.ERRORS.MISSING_PARAM'));
            return $response;
        }
        $request_array = json_decode($this->request->get('request'));

        if (!$request_array['bot_vk_id'] &&
            !$request_array['bot_fb_id'] &&
            !$request_array['bot_telegram_id'] &&
            !$request_array['bot_skype_id'] &&
            !$request_array['bot_viber_id']) {
            $response->setJsonContent(array('error' => 'GENERAL.ERRORS.MISSING_BOT'));
            return $response;
        }
        if (!$request_array['message']) {
            $response->setJsonContent(array('error' => 'GENERAL.ERRORS.MISSING_MESSAGE'));
            return $response;
        }

        $uid = $this->session->get('uid');
        $data_array = array();

        $data_array['id'] = intval($request_array['id']);
        $data_array['uid'] = $uid;

        self::_dispatchFormDataArrayFromRequest($data_array, $request_array);

        $array_to_check = array(
            array("bot_id" => $data_array['bot_vk_id'], "bot_enum_id" => VK_ID),
            array("bot_id" => $data_array['bot_fb_id'], "bot_enum_id" => FB_ID),
            array("bot_id" => $data_array['bot_telegram_id'], "bot_enum_id" => TELEGRAM_ID),
            array("bot_id" => $data_array['bot_skype_id'], "bot_enum_id" => SKYPE_ID),
            array("bot_id" => $data_array['bot_viber_id'], "bot_enum_id" => VIBER_ID),

        );
        if (!BotsController::checkUserAccessToBotsByBotIdBatch($array_to_check, $uid)) {
            $response->setJsonContent(array('error' => 'GENERAL.ERRORS.ACCES_DENIED'));
            return $response;
        }


        if ($request_array['message']['message_id'] && intval($request_array['message']['message_id'])) {
            $message = Message::findFirstById(intval($request_array['message']['message_id']));
            if (!$message) {
                $response->setJsonContent(array('error' => 'GENERAL.ERRORS.WRONG_MESSAGE_ID'));
                return $response;
            }
            if ($message->uid != $uid) {
                $response->setJsonContent(array('error' => 'GENERAL.ERRORS.ACCES_DENIED'));
                return $response;
            }
            $message_json = json_encode($request_array['message']['json']);
            if (Message::checkMessageJson($message_json)) {
                $message->json = $message_json;
                $success = $message->update();
                if (!$success) {
                    $response->setJsonContent(array('error' => 'GENERAL.ERRORS.CANT_UPDATE_MESSAGE'));
                    return $response;
                }
            } else {
                $response->setJsonContent(array('error' => 'GENERAL.ERRORS.NOT_VALID_MESSAGE'));
                return $response;
            }
        }

        $result = Dispatch::updateUserDispatch($data_array);
        if (is_array($result) && isset($result['error'])) {
            $response->setJsonContent($result);
            return $response;
        }

        $response->setJsonContent($result);
        return $response;
    }

    /**
     * @api {post} dispatch/activateDispatch activateDispatch
     * @apiName activateDispatch
     * @apiGroup Dispatch
     * @apiPermission Аутентифицированные
     * @apiDescription включает рассылку
     * @apiParam {string} id ид расслыки
     * @apiUse SecurityCheck
     * @apiSuccess {json} result -> success
     */
    public function activateDispatchAction()
    {
        $result = array();
        $uid = $this->session->get('uid');
        $dispatch_id = intval($this->request->get('id'));
        $response = $this->prepareResponse();
        if ($dispatch_id) {
            $dispatch = Dispatch::findFirstById($dispatch_id);
            if ($dispatch->uid == $uid) {
                $dispatch->activateDispatch();
                $response->setJsonContent(["result" => "success"]);
            } else {
                $response->setJsonContent(array('error' => 'GENERAL.ERRORS.ACCESS_DENIED'));
                return $response;
            }
        } else {
            $response->setJsonContent(array('error' => 'GENERAL.ERRORS.MISSING_PARAM'));
            return $response;
        }


        return $response;
    }

    /**
     * @api {post} dispatch/deactivateDispatch deactivateDispatch
     * @apiName deactivateDispatch
     * @apiGroup Dispatch
     * @apiPermission Аутентифицированные
     * @apiDescription отключает рассылку
     * @apiParam {string} id ид расслыки
     * @apiUse SecurityCheck
     * @apiSuccess {json} result -> success
     */
    public function deactivateDispatchAction()
    {
        $result = array();
        $uid = $this->session->get('uid');
        $dispatch_id = intval($this->request->get('id'));
        $response = $this->prepareResponse();
        if ($dispatch_id) {
            $dispatch = Dispatch::findFirstById($dispatch_id);
            if ($dispatch->uid == $uid) {
                $dispatch->deactivateDispatch();
                $response->setJsonContent(["result" => "success"]);
            } else {
                $response->setJsonContent(array('error' => 'GENERAL.ERRORS.ACCESS_DENIED'));
                return $response;
            }
        } else {
            $response->setJsonContent(array('error' => 'GENERAL.ERRORS.MISSING_PARAM'));
            return $response;
        }


        return $response;
    }

    /**
     * @api {post} dispatch/deleteDispatch deleteDispatch
     * @apiName deleteDispatch
     * @apiGroup Dispatch
     * @apiPermission Аутентифицированные
     * @apiDescription удаляет рассылку
     * @apiParam {string} id ид рассылки для удаления
     * @apiUse SecurityCheck
     * @apiSuccess {json} result -> success
     */
    public function deleteDispatchAction()
    {
        $response = $this->prepareResponse();
        $uid = $this->session->get('uid');
        $dispatch_id = intval($this->request->get('id'));
        if ($dispatch_id && Dispatch::checkUserPermission($uid, $dispatch_id)) {
            if ($result = Dispatch::deleteRecord($dispatch_id)) {
                $response->setJsonContent($result);
                return $response;
            }
        } else {
            $response->setJsonContent(array('error' => 'GENERAL.ERRORS.ACCESS_DENIED'));
            return $response;
        }
    }


}