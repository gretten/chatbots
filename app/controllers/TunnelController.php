<?php

/**
 * Контроллер для работы с тоннелями
 * Class AuthController
 */
class TunnelController extends ControllerBase
{
    public function indexAction()
    {
        $response = $this->prepareResponse();


        $result_array = array();
        $uid = $this->session->get('uid');
        $campaing_id = $this->request->get('campaing_id');
        if (Campaign::checkUserPermission($uid, $campaing_id)) {

            $tunnelBlocksArray = TunnelBlocks::getTunnelBlocks($campaing_id);
            $tunnelBlocksTreeArray = TunnelBlocksTree::findTunnelBlocksParents(array_keys($tunnelBlocksArray));
            foreach ($tunnelBlocksTreeArray as $key => &$treeItem) {
                if (isset($tunnelBlocksArray[$key])) {
                    $treeItem['block'] = $tunnelBlocksArray[$key];
                }
            }
            $result_array = $tunnelBlocksTreeArray;

            $response->setJsonContent($result_array);
            return $response;
        } else {
            $response->setJsonContent(array('error' => 'GENERAL.ERRORS.ACCESS_DENIED'));
            return $response;
        }


    }

    public function addTunnelBlockAction()
    {

    }

    public function updateTunnelBlockAction()
    {

    }

    public function deleteTunnelBlockAction()
    {

    }

}