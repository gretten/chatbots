<?php

/**
 * Контроллер для работы с виджетом первого входа
 * Class FirstentryController
 */
class FirstentryController extends ControllerBase
{
    /**
     * @api {post} firstentry/ firstentrySave
     * @apiName firstentrySave
     * @apiGroup firstentry
     * @apiPermission Аутентифицированные
     * @apiDescription Сохраняет информацию от виджета первого захода
     * @apiParam {json} request Объект  с кучей данных (описание в тайге)
     * @apiUse SecurityCheck
     * @apiSuccess {json} result -> success
     */
    public function indexAction()
    {


        $response = $this->prepareResponse();
        $json_to_operate = $this->request->get('request');

        $uid = $this->session->get('uid');

        if ($json_to_operate) {

            $json_to_operate = json_decode($json_to_operate, true);

            if (!$json_to_operate) {
                $response->setJsonContent(array('error' => 'GENERAL.ERRORS.CANT_PARSE_JSON'));
                return $response;
            }
            //cоздаем нужного бота
            $new_bot = new stdClass();

            if ($json_to_operate['bot'] && $json_to_operate['greeting_message']) {
                $new_bot = BotsController::_botsAddBot($json_to_operate['bot'], $uid);
                if (is_array($new_bot) && isset($new_bot['error'])) {
                    $response->setJsonContent($new_bot);
                    return $response;
                }
                if (!$new_bot) {
                    $response->setJsonContent(array('error' => 'GENERAL.ERRORS.CANT_CREATE_GREETING_MESSAGE'));
                    return $response;
                }
            } else {
                $response->setJsonContent(array('error' => 'GENERAL.ERRORS.MISSING_PARAM'));
                return $response;
            }
            // пихаем ид новго бота, создаем приветственное сообщение
            if ($new_bot->id) {
                $message_id = GreetingMessageSettings::addGreetingMessage($new_bot->id, $uid, $json_to_operate['bot']['type'], json_encode($json_to_operate['greeting_message']['json']));
                if (!$message_id) {
                    $response->setJsonContent(array('error' => 'GENERAL.ERRORS.CANT_CREATE_GREETING_MESSAGE'));
                    return $response;
                }
            } else {
                $response->setJsonContent(array('error' => 'GENERAL.ERRORS.CANT_CREATE_BOT'));
                return $response;
            }
            //создаем новую рассылку, если надо
            if ($json_to_operate['dispatch']) {
                $data_array = $json_to_operate['dispatch'];
                $data_array['uid'] = $uid;
                $json_to_operate['dispatch']['message']['json']['bot_type_id'] = $json_to_operate['bot']['type'];

                unset($data_array['message']);
                switch ($json_to_operate['bot']['type']) {
                    case VK_ID:
                        $data_array['bot_vk_id'] = (int)$new_bot->id;
                        break;
                    case FB_ID:
                        $data_array['bot_fb_id'] = (int)$new_bot->id;
                        break;
                    case TELEGRAM_ID:
                        $data_array['bot_telegram_id'] = (int)$new_bot->id;
                        break;
                    case SKYPE_ID:
                        $data_array['bot_skype_id'] = (int)$new_bot->id;
                        break;
                    case VIBER_ID:
                        $data_array['bot_viber_id'] = (int)$new_bot->id;
                        break;
                }

                $result = DispatchController::_dispatchAddDispatch($uid, json_encode($json_to_operate['dispatch']['message']['json']), $data_array);
                if (is_array($result) && $result['error']) {
                    $response->setJsonContent($result);
                    return $response;
                }
            }


        } else {
            $response->setJsonContent(array('error' => 'GENERAL.ERRORS.MISSING_PARAM'));
            return $response;
        }

        $response->setJsonContent(array("result" => "success"));
        return $response;

    }


}
