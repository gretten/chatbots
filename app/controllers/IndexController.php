<?php

class IndexController extends ControllerBase {

    public function pingAction() {
        $response = $this->prepareResponse();
        $response->setJsonContent(array(
            'session_id' => $this->session->getId()
        ));
        return $response;
    }
}

