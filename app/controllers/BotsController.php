<?php

/**
 * Контроллер для работы с ботами
 * Class BotsController
 */
class BotsController extends ControllerBase
{
    /**
     * @api {get} bots/ index
     * @apiName index
     * @apiGroup Bots
     * @apiPermission Аутентифицированные пользователи
     * @apiDescription Получает информацию о всех ботах пользователя
     * @apiParam {string} uid (Optional) id пользователя в случае, если он просматривает не свою страницу, работать не будет до введение ролей
     * @apiParam {json} fiters (Optional) массиф фильтров, работать не будет до окончания беты
     * @apiSuccess {json} bots массив ботов пользователя
     */
    public function indexAction()
    {
        $response = $this->prepareResponse();
        $result_array = array();
        $uid = $this->session->get('uid');
        // $uid = (!$this->request->get('uid'))?$this->session->get('uid'): $uid = $this->request->get('uid');
        $bot_array = array();
        $bot_array['fb'] = BotFbSettings::find(array('uid = :uid:', 'bind' => array('uid' => $uid)));
        $bot_array['telegram'] = BotTelegramSettings::find(array('uid = :uid:', 'bind' => array('uid' => $uid)));
        $bot_array['vk'] = BotVkSettings::find(array('uid = :uid:', 'bind' => array('uid' => $uid)));
        $bot_array['viber'] = BotViberSettings::find(array('uid = :uid:', 'bind' => array('uid' => $uid)));
        $bot_array['skype'] = BotSkypeSettings::find(array('uid = :uid:', 'bind' => array('uid' => $uid)));

        $response->setJsonContent($result_array);
        return $response;

    }


    /**
     * @api {get} bots/greetingMessages greetingMessages
     * @apiName greetingMessages
     * @apiGroup Bots
     * @apiPermission Аутентифицированные пользователи
     * @apiDescription Получает информацию о приветственных сообщениях бота
     * @apiParam {string} uid (Optional) id пользователя в случае, если он просматривает не свою страницу, работать не будет до введение ролей
     * @apiParam {Number} botId ид бота
     * @apiParam {string} typeEnum тип бота
     * @apiSuccess {json} messages массив приветственных сообщений бота
     */
    public function greetingMessagesAction()
    {
        $response = $this->prepareResponse();


        $result_array = array();
        $uid = $this->session->get('uid');

        // $uid = (!$this->request->get('uid'))?$this->session->get('uid'): $uid = $this->request->get('uid');
        if ($this->request->get('botId') && $this->request->get('type')) {
            $bot_id = $this->request->get('botId');
            $bot_enum_id = $this->request->get('type');
            if (!self::checkUserAccessToBotsByBotId($bot_id, $bot_enum_id, $uid)) {
                $response->setJsonContent(array('error' => 'GENERAL.ERRORS.ACCESS_DENIED'));
                return;
            }
            $result_array = GreetingMessageSettings::getBotGreetingsMessages($bot_id, $bot_enum_id, $uid);

        } else {
            $response->setJsonContent(array('error' => 'GENERAL.ERRORS.MISSING_PARAMETR'));
            return $response;
        }

        $response->setJsonContent($result_array);
        return $response;

    }

    /**
     * @api {post} bots/addBot addBot
     * @apiName addBot
     * @apiGroup Bots
     * @apiPermission Аутентифицированные пользователи
     * @apiDescription Добавление бота (настроек) пользователю
     * @apiParam {string} uid (Optional) id пользователя в случае, если он просматривает не свою страницу, работать не будет до введения ролей
     * @apiParam {json} bot массив с заполненными полями (для разных типов ботов нужны разные поля)
     * @apiSuccess {json} bot json добавленного бота с присвоенным id
     */
    public function addBotAction()
    {
        $response = $this->prepareResponse();


        $result_array = array();
        $session_uid = $this->session->get('uid');
        $uid = (!$this->request->get('uid')) ? $this->session->get('uid') : $this->request->get('uid');
        if ($session_uid != $uid) {
            //если пользователь не имеет доступа к добавлению ботов в личном кабинете другого пользователя, то говорим что бота он создает самому себе
            if (!self::checkUserAccessToBots($session_uid, $uid)) $uid = $session_uid;
        }
        if ($bot = $this->request->get('bot')) {
            $bot = json_decode($bot, true);
            if (isset($bot['name'])) {
                $result_array = self::_botsAddBot($bot, $uid);
            }
            $response->setJsonContent($result_array);
        } else {
            $response->setJsonContent(array('error' => 'GENERAL.ERRORS.MISSING_PARAMETR'));
        }


        return $response;

    }

    public static function _botsAddBot($bot, $uid)
    {
        if (!is_array($bot)) {
            $bot = (array)$bot;
        }

        $result_array = array();
        switch ($bot['type']) {
            case FB_ID:
                if (isset($bot['token'])) {
                    $new_bot = new BotFbSettings();
                    $new_bot_settings = array(
                        "id" => "",
                        "uid" => $uid,
                        "name" => $bot['name'],
                        "token" => $bot['token'],
                        "registered" => 0,
                    );
                    $success = $new_bot->create($new_bot_settings);
                    if ($success) {
                        $result_array = $new_bot;
                    }
                }
                break;
            case VK_ID:
                if (isset($bot['group_id']) && isset($bot['token'])) {
                    $new_bot = new BotVkSettings();
                    $new_bot_settings = array(
                        "id" => "",
                        "uid" => $uid,
                        "name" => $bot['name'],
                        "group_id" => $bot['group_id'],
                        "token" => $bot['token'],
                        "registered" => 0,
                    );
                    $success = $new_bot->create($new_bot_settings);
                    if ($success) {
                        $result_array = $new_bot;
                    }
                }
                break;
            case SKYPE_ID:
                if (isset($bot['app_name']) && isset($bot['app_id']) && isset($bot['app_pass']) && isset($bot['message_endpoint'])) {
                    $new_bot = new BotSkypeSettings();
                    $new_bot_settings = array(
                        "id" => "",
                        "uid" => $uid,
                        "name" => $bot['name'],
                        "app_name" => $bot['app_name'],
                        "app_id" => $bot['app_id'],
                        "app_pass" => $bot['app_pass'],
                        "message_endpoint" => $bot['message_endpoint'],
                        "registered" => 0,
                    );
                    $success = $new_bot->create($new_bot_settings);
                    if ($success) {
                        $result_array = $new_bot;
                    }
                }
                break;
            case VIBER_ID:
                if (isset($bot['token']) && isset($bot['tech_name'])) {
                    $new_bot = new BotViberSettings();
                    $new_bot_settings = array(
                        "id" => "",
                        "uid" => $uid,
                        "name" => $bot['name'],
                        "token" => $bot['token'],
                        "tech_name" => $bot['tech_name'],
                        "registered" => 0,
                    );
                    $success = $new_bot->create($new_bot_settings);
                    if ($success) {
                        $result_array = $new_bot;
                    }
                }
                break;
            case TELEGRAM_ID:
                if (isset($bot['token'])) {
                    $new_bot = new BotTelegramSettings();
                    $new_bot_settings = array(
                        "id" => "",
                        "uid" => $uid,
                        "name" => $bot['name'],
                        "token" => $bot['token'],
                        "registered" => 0,
                    );

                    $success = $new_bot->create($new_bot_settings);

                    if ($success) {
                        $result_array = $new_bot;
                    }
                }
                break;
            default:
                $result_array = array('error' => 'GENERAL.ERRORS.INCORRECT_TYPE');

                break;
        }
        return $result_array;
    }


    /**
     * @api {post} bots/updateBot updateBot
     * @apiName updateBot
     * @apiGroup Bots
     * @apiPermission Аутентифицированные пользователи
     * @apiDescription Обновление информации о боте
     * @apiSuccess {json} response "response->success" в случае успеха
     * @apiParam {json} bot массив с заполненными полями (для разных типов ботов нужны разные поля)
     */
    public function updateBotAction()
    {
        $response = $this->prepareResponse();


        $result_array = array("response" => "success");
        $uid = $this->session->get('uid');
        //$uid = (!$this->request->get('uid')) ? $this->session->get('uid') : $this->request->get('uid');
        if ($bot = $this->request->get('bot')) {
            $bot = json_decode($bot, true);
            if (isset($bot['id'])) {
                if (!self::checkUserAccessToBotsByBotId($bot['id'], $bot['type'], $uid)) {
                    $response->setJsonContent(array('error' => 'GENERAL.ERRORS.ACCESS_DENIED'));
                    return;
                }
                switch ($bot['type']) {
                    case FB_ID:
                        $new_bot = BotFbSettings::findFirstById($bot['id']);
                        if (isset($bot['name']) && $bot['name']) {
                            $new_bot->name = $bot['name'];
                        }
                        if (isset($bot['token']) && $bot['token']) {
                            $new_bot->token = $bot['token'];
                        }

                        $success = $new_bot->update();
                        if (!$success) {
                            $result_array = array('error' => 'GENERAL.ERRORS.INCORRECT_TYPE');
                            break;
                        }

                        break;
                    case VK_ID:
                        $new_bot = BotVkSettings::findFirstById($bot['id']);

                        if (isset($bot['name']) && $bot['name']) {
                            $new_bot->name = $bot['name'];
                        }
                        if (isset($bot['token']) && $bot['token']) {
                            $new_bot->token = $bot['token'];
                        }
                        if (isset($bot['group_id']) && $bot['group_id']) {
                            $new_bot->group_id = $bot['group_id'];
                        }

                        $success = $new_bot->update();
                        if (!$success) {
                            $result_array = array('error' => 'GENERAL.ERRORS.INCORRECT_TYPE');
                            break;
                        }

                        break;
                    case SKYPE_ID:
                        $new_bot = BotSkypeSettings::findFirstById($bot['id']);

                        if (isset($bot['name']) && $bot['name']) {
                            $new_bot->name = $bot['name'];
                        }
                        if (isset($bot['app_name']) && $bot['app_name']) {
                            $new_bot->app_name = $bot['app_name'];
                        }
                        if (isset($bot['app_id']) && $bot['app_id']) {
                            $new_bot->app_id = $bot['app_id'];
                        }
                        if (isset($bot['app_pass']) && $bot['app_pass']) {
                            $new_bot->app_pass = $bot['app_pass'];
                        }
                        if (isset($bot['message_endpoint']) && $bot['message_endpoint']) {
                            $new_bot->message_endpoint = $bot['message_endpoint'];
                        }

                        $success = $new_bot->update();
                        if (!$success) {
                            $result_array = array('error' => 'GENERAL.ERRORS.INCORRECT_TYPE');
                            break;
                        }

                        break;
                    case VIBER_ID:
                        $new_bot = BotViberSettings::findFirstById($bot['id']);

                        if (isset($bot['name']) && $bot['name']) {
                            $new_bot->name = $bot['name'];
                        }
                        if (isset($bot['token']) && $bot['token']) {
                            $new_bot->token = $bot['token'];
                        }
                        if (isset($bot['tech_name']) && $bot['tech_name']) {
                            $new_bot->tech_name = $bot['tech_name'];
                        }

                        $success = $new_bot->update();
                        if (!$success) {
                            $result_array = array('error' => 'GENERAL.ERRORS.INCORRECT_TYPE');
                            break;
                        }

                        break;
                    case TELEGRAM_ID:
                        $new_bot = BotTelegramSettings::findFirstById($bot['id']);

                        if (isset($bot['name']) && $bot['name']) {
                            $new_bot->name = $bot['name'];
                        }
                        if (isset($bot['token']) && $bot['token']) {
                            $new_bot->token = $bot['token'];
                        }


                        $success = $new_bot->update();
                        if (!$success) {
                            $result_array = array('error' => 'GENERAL.ERRORS.INCORRECT_TYPE');
                            break;
                        }

                        break;
                    default:
                        $response->setJsonContent(array('error' => 'GENERAL.ERRORS.INCORRECT_TYPE'));
                        return $response;
                        break;
                }
            }
            $response->setJsonContent($result_array);
        } else {
            $response->setJsonContent(array('error' => 'GENERAL.ERRORS.MISSING_ID'));
        }


        return $response;

    }

    /**
     * @api {post} bots/deleteBot deleteBot
     * @apiName deleteBot
     * @apiGroup Bots
     * @apiPermission Аутентифицированные пользователи
     * @apiDescription Удаление бота
     * @apiSuccess {json} response "response->success" в случае успеха
     * @apiParam {string} id ид бота для удаления
     * @apiParam {string} type тип бота (фб вк и далее)
     */
    public function deleteBotAction()
    {
        $response = $this->prepareResponse();


        $result_array = array("response" => "success");
        $uid = $this->session->get('uid');
        if ($this->request->get('id') && $this->request->get('type')) {
            $new_bot = new stdClass();;
            switch ($this->request->get('type')) {
                case FB_ID:
                    $new_bot = BotFbSettings::findFirstById($this->request->get('id'));
                    break;
                case VK_ID:
                    $new_bot = BotVkSettings::findFirstById($this->request->get('id'));
                    break;
                case SKYPE_ID:
                    $new_bot = BotSkypeSettings::findFirstById($this->request->get('id'));
                    break;
                case VIBER_ID:
                    $new_bot = BotViberSettings::findFirstById($this->request->get('id'));
                    break;
                case TELEGRAM_ID:
                    $new_bot = BotTelegramSettings::findFirstById($this->request->get('id'));
                    break;
            }
            if (isset($new_bot->uid) && self::checkUserAccessToBots($new_bot->uid, $uid)) {
                $new_bot->delete();
                $response->setJsonContent($result_array);
            } else {
                $response->setJsonContent(array('error' => 'GENERAL.ERRORS.ACCESS_DENIED'));
            }


        } else {
            $response->setJsonContent(array('error' => 'GENERAL.ERRORS.MISSING_ID'));
        }


        return $response;

    }

    /**
     * @api {post} bots/updateBotGreetings updateBotGreetings
     * @apiName updateBotGreetings
     * @apiGroup Bots
     * @apiPermission Аутентифицированные пользователи
     * @apiDescription Обновление приветственных сообщений для бота
     * @apiParam {json} message массив с заполненными полями
     * @apiParam {Number} botId id к которому относится это изменение
     * @apiSuccess {json} response "response->success" в случае успеха
     */
    public function updateBotGreetingsAction()
    {
        $response = $this->prepareResponse();


        $uid = $this->session->get('uid');
        // $uid = (!$this->request->get('uid'))?$this->session->get('uid'): $uid = $this->request->get('uid');
        //создаем сообщение
        if ($this->request->get('botId') && $this->request->get('message')) {
            $message_id = GreetingMessageSettings::updateGreetingMessage($this->request->get('message'), $uid, $this->request->get('botId'));
            if ($message_id) {
                $response->setJsonContent(array('response' => "success"));
            } else {
                $response->setJsonContent(array('error' => 'GENERAL.ERRORS.MISSING_PARAMETR'));
            }
        } else {
            $response->setJsonContent(array('error' => 'GENERAL.ERRORS.MISSING_PARAMETR'));
        }
        return $response;
    }

    /**
     * @api {post} bots/addBotGreetings addBotGreetings
     * @apiName addBotGreetings
     * @apiGroup Bots
     * @apiPermission Аутентифицированные пользователи
     * @apiDescription Добавление приветственных сообщений для бота
     * @apiParam {json} message массив с заполненными полями
     * @apiParam {Number} botId id к которому относится это изменение
     * @apiParam {string} type тип бота (фб вк и далее)
     * @apiSuccess {Number} messageId номер сообщения
     */
    public function addBotGreetingsAction()
    {
        $response = $this->prepareResponse();


        $uid = $this->session->get('uid');
        // $uid = (!$this->request->get('uid'))?$this->session->get('uid'): $uid = $this->request->get('uid');
        //создаем сообщение
        if ($this->request->get('botId') && $this->request->get('type') && $this->request->get('greetingMessage')) {
            $message_id = GreetingMessageSettings::addGreetingMessage($this->request->get('botId'), $uid, $this->request->get('type'), $this->request->get('greetingMessage'));
            if ($message_id) {
                $response->setJsonContent(array('messageId' => $message_id));
            } else {
                $response->setJsonContent(array('error' => 'GENERAL.ERRORS.MISSING_PARAMETR'));
            }
        } else {
            $response->setJsonContent(array('error' => 'GENERAL.ERRORS.MISSING_PARAMETR'));
        }


        return $response;

    }

    /**
     * @api {post} bots/deleteBotGreetings deleteBotGreetings
     * @apiName deleteBotGreetings
     * @apiGroup Bots
     * @apiPermission Аутентифицированные пользователи
     * @apiDescription удаление приветственных сообщений для бота
     * @apiParam {json} messageId (Optional) если не заданно - удаляются все сообщения относящиеся к боту
     * @apiParam {Number} botId id к которому относится это изменение
     * @apiSuccess {json}response "response->success" в случае успеха
     */
    public function deleteBotGreetingsAction()
    {
        $response = $this->prepareResponse();


        $uid = $this->session->get('uid');
        // $uid = (!$this->request->get('uid'))?$this->session->get('uid'): $uid = $this->request->get('uid');
        //создаем сообщение
        if ($bot_id = $this->request->get('botId')) {
            if ($message_id = $this->request->get('messageId')) {
                $success = GreetingMessageSettings::deleteGreetingMessage($message_id);
                if ($success) {
                    $response->setJsonContent(array('response' => "success"));
                }
            } else {
                $success = GreetingMessageSettings::deleteAllGreetingMessages($bot_id);
                if ($success) {
                    $response->setJsonContent(array('response' => "success"));
                }
            }


        } else {
            $response->setJsonContent(array('error' => 'GENERAL.ERRORS.MISSING_PARAMETR'));
        }
        return $response;

    }

    public static function checkUserAccessToBotsByBotIdBatch($data_array, $uid)
    {
        foreach ($data_array as $data_item) {
            if ($data_item['bot_id']) {
                if (!self::checkUserAccessToBotsByBotId($data_item['bot_id'], $data_item['bot_enum_id'], $uid)) return false;
            }
        }
        return true;
    }


    public static function checkUserAccessToBotsByBotId($bot_id, $bot_enum_id, $uid)
    {
        switch ($bot_enum_id) {
            case VK_ID:
                $new_bot = BotVkSettings::findFirstById($bot_id);
                if (!self::checkUserAccessToBots($new_bot->uid, $uid)) {
                    return false;
                };
                break;
            case FB_ID:
                $new_bot = BotFbSettings::findFirstById($bot_id);
                if (!self::checkUserAccessToBots($new_bot->uid, $uid)) {
                    return false;
                };
                break;

            case VIBER_ID:
                $new_bot = BotViberSettings::findFirstById($bot_id);
                if (!self::checkUserAccessToBots($new_bot->uid, $uid)) {
                    return false;
                };
                break;

            case SKYPE_ID:
                $new_bot = BotSkypeSettings::findFirstById($bot_id);
                if (!self::checkUserAccessToBots($new_bot->uid, $uid)) {
                    return false;
                };
                break;

            case TELEGRAM_ID:
                $new_bot = BotTelegramSettings::findFirstById($bot_id);
                if (!self::checkUserAccessToBots($new_bot->uid, $uid)) {
                    return false;
                };
                break;


        }
        return true;
    }

    //вроде глупость, но пригодится, когда будем делать роли
    public static function checkUserAccessToBots($uid_in_bot, $uid_of_user)
    {
        if ($uid_in_bot == $uid_of_user) {
            return true;
        }
        return false;
    }
}