<?php

class ErrorsController extends ControllerBase {

    public function indexAction($error) {
        $response = $this->prepareResponse();
        $response->setJsonContent(array('error' => $error));
        return $response;
    }

}

