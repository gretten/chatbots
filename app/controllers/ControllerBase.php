<?php

use Phalcon\Mvc\Controller;
use Phalcon\Http\Response;

class ControllerBase extends Controller {

    /**
     * Подготавливает контроллер для возвращения данных в json-формате
     * @return Response
     */
    public function prepareResponse() {
        $this->view->disable();
        $response = new Response();
        $origin = $this->request->getHttpHost();
        $headers = array();
        if (function_exists('apache_request_headers')) {
            $headers = apache_request_headers();
        } else {
            foreach ($_SERVER as $key => $value) {
                if (substr($key, 0, 5) == "HTTP_") {
                    $key = str_replace(" ", "-", ucwords(strtolower(str_replace("_", " ", substr($key, 5)))));
                    $headers[$key] = $value;
                } else {
                    $headers[$key] = $value;
                }
            }
        }
        if (isset($headers['Origin'])) $origin = $headers['Origin'];
        $response->setHeader('Access-Control-Allow-Origin', $origin);
        $response->setHeader('Access-Control-Allow-Methods', 'POST, GET, OPTIONS');
        $response->setHeader('Access-Control-Allow-Headers', 'X-Requested-With,Content-Type');
        $response->setHeader('Access-Control-Allow-Credentials', 'true');
        $response->setHeader('Content-Type', 'text/javascript');
        $response->setHeader('P3P', 'CP="CAO PSA OUR"');
        $response->setHeader("Content-Type", "application/json");
        return $response;
    }

    /**
     * Возвращает объект с post-параметрами
     * @return object
     */
    public function getParams() {
        $data = $this->request->getJsonRawBody();
        if (!$data) {
            $fields = $this->request->getPost();
            if (!count($fields)) {
                return $data;
            }
            $data = (object) array();
            foreach ($fields as $field => $value) {
                $data->$field = $value;
            }
        }
        return $data;
    }
}
