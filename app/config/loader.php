<?php

$loader = new \Phalcon\Loader();

/**
 * We're a registering a set of directories taken from the configuration file
 */
$loader->registerDirs(
    [
        $config->application->controllersDir,
        $config->application->modelsDir,
        $config->application->pluginsDir,
        $config->application->serviceDir,
        $config->application->validatorsDir
    ]
)->register();
/*try {
    require_once $config->application->appDir . '../vendor/composer/autoload_real.php';
} catch (Exception $e) {
    var_dump($e);die();
}*/

