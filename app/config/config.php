<?php
/*
 * Modified: prepend directory path of current file, because of this file own different ENV under between Apache and command line.
 * NOTE: please remove this comment.
 */
defined('BASE_PATH') || define('BASE_PATH', getenv('BASE_PATH') ?: realpath(dirname(__FILE__) . '/../..'));
defined('APP_PATH') || define('APP_PATH', BASE_PATH . '/app');
//хардкодим идшники до лучших времен
defined('FB_ID') || define('FB_ID', 1);
defined('TELEGRAM_ID') || define('TELEGRAM_ID', 2);
defined('VIBER_ID') || define('VIBER_ID', 3);
defined('SKYPE_ID') || define('SKYPE_ID', 4);
defined('VK_ID') || define('VK_ID', 5);

return new \Phalcon\Config([
    'database' => [
        'adapter'     => 'Mysql',
        'host'        => 'galdias.cimlancahzij.eu-central-1.rds.amazonaws.com',
        'username'    => 'galdias',
        'password'    => 'zaq12wsx',
        'dbname'      => 'chatbots',
        'charset'     => 'utf8',
    ],
    /*'oauth' => [
        'adapter'  => 'Mysql',
        'host'     => 'localhost',
        'username' => 'armelle_main',
        'password' => 'F3d0D2o6',
        'dbname'   => 'oauth2',
    ],*/
    'mailer' => [
        'driver'    => 'mail',
        'from'     => [
            'email' => 'info@chainbot.ru',
            'name'  => 'Команда Chainbot'
        ]
    ],
    'application' => [
        'appDir'         => APP_PATH . '/',
        'controllersDir' => APP_PATH . '/controllers/',
        'modelsDir'      => APP_PATH . '/models/',
        'migrationsDir'  => APP_PATH . '/migrations/',
        'viewsDir'       => APP_PATH . '/views/',
        'pluginsDir'     => APP_PATH . '/plugins/',
        'libraryDir'     => APP_PATH . '/library/',
        'cacheDir'       => BASE_PATH . '/cache/',
        'serviceDir'     => APP_PATH . '/services/',
        'validatorsDir'  => APP_PATH . '/validators',
        'projectUrl'     => 'https://chainbot.ru/',
        // This allows the baseUri to be understand project paths that are not in the root directory
        // of the webpspace.  This will break if the public/index.php entry point is moved or
        // possibly if the web server rewrite rules are changed. This can also be set to a static path.
        'baseUri'        => preg_replace('/public([\/\\\\])index.php$/', '', $_SERVER["PHP_SELF"]),
    ]
]);
