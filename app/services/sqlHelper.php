<?php

use Phalcon\Db;

/**
 * Вспомогательный класс для прямой работы с БД
 * Class sqlHelper
 */
class sqlHelper {
    /**
     * Возвращает результат sql-запроса
     * @param $sql string
     * @param $params array
     * @return mixed
     */
    public static function getSqlData($sql, $params=array()) {
        $result = Phalcon\DI::getDefault()->getShared('db')->query($sql, $params);
        $result->setFetchMode(Db::FETCH_OBJ);
        return $result->fetchAll();
    }

    /**
     * Возвращает одну строку
     * @param $sql string
     * @param $params array
     * @return mixed
     */
    public static function getSqlRow($sql, $params=array()) {
        $result = Phalcon\DI::getDefault()->getShared('db')->query($sql, $params);
        $result->setFetchMode(Db::FETCH_OBJ);
        return $result->fetch();
    }

    /**
     * Выполняет sql-запрос
     * @param $sql
     * @param array $params
     * @return mixed
     */
    public static function executeSql($sql, $params=array()) {
        $connection = Phalcon\DI::getDefault()->getShared('db');
        if ($connection->query($sql, $params)) {
            if ($connection->lastInsertId()) {
                return $connection->lastInsertId();
            } else {
                return $connection->affectedRows();
            }
        } else {
            return false;
        }
    }
}