<?php

use Phalcon\Acl;
use Phalcon\Acl\Role;
use Phalcon\Acl\Resource;
use Phalcon\Events\Event;
use Phalcon\Mvc\User\Plugin;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Acl\Adapter\Memory as AclList;

/**
 * SecurityPlugin
 *
 * This is the security plugin which controls that users only have access to the modules they're assigned to
 */
class SecurityPlugin extends Plugin
{

    /**
     * Возвращает схему прав доступа
     * @returns AclList
     */
    public function getAcl()
    {

        $acl = new AclList();
        $acl->setDefaultAction(Acl::DENY);

        /* Определение ролей */
        $guestsRole = new Role('Guests', 'Неавторизированный пользователь');
        $usersRole = new Role('Users', 'Клиент/Дистрибьютор');
        $storeKeepersRole = new Role('Storekeepers', 'Кладовщик');
        $supervisorsRole = new Role('Supervisors', 'Менеджер/Администратор');

        $acl->addRole($guestsRole);
        $acl->addRole($usersRole, $guestsRole);
        $acl->addRole($storeKeepersRole, $guestsRole);
        $acl->addRole($supervisorsRole, $usersRole);

        /* Права todo: хранить в БД */
        $resources = array(
            'index' => array('ping'),
            'files' => array('data'),
            'auth' => array('credentials', 'ping', 'logout', 'register', 'remindPassword', 'recoverAndChangePassword'),
            'bots' => array('index', 'addBot', 'updateBot', 'deleteBot', 'updateBotGreetings', 'addBotGreetings', 'deleteBotGreetings'),
            'tunnel' => array('index'),
            'messages' => array('index'),
            'errors' => array('index'),
            'firstentry' => array('index'),
            'dispatch' => array('activateDispatch','deactivateDispatch', 'index', 'addDispatch', 'updateDispatch', 'deleteDispatch')

        );

        foreach ($resources as $resource => $actions) {
            $acl->addResource(new Resource($resource), $actions);
        }

        $acl->allow('Guests', 'auth', 'ping');
        $acl->allow('Guests', 'auth', 'credentials');
        $acl->allow('Guests', 'auth', 'register');
        $acl->allow('Guests', 'auth', 'remindPassword');
        $acl->allow('Guests', 'auth', 'recoverAndChangePassword');
        $acl->allow('Guests', 'errors', 'index');
        $acl->allow('Guests', 'tunnel', 'index');
        $acl->allow('Guests', 'dispatch', 'index');
        $acl->allow('Guests', 'dispatch', 'addDispatch');
        $acl->allow('Guests', 'dispatch', 'updateDispatch');
        $acl->allow('Guests', 'dispatch', 'deleteDispatch');
        $acl->allow('Guests', 'dispatch', 'activateDispatch');
        $acl->allow('Guests', 'dispatch', 'deactivateDispatch');
        $acl->allow('Guests', 'bots', 'index');
        $acl->allow('Guests', 'bots', 'addBot');
        $acl->allow('Guests', 'bots', 'updateBot');
        $acl->allow('Guests', 'bots', 'deleteBot');
        $acl->allow('Guests', 'bots', 'updateBotGreetings');
        $acl->allow('Guests', 'bots', 'addBotGreetings');
        $acl->allow('Guests', 'bots', 'deleteBotGreetings');
        $acl->allow('Guests', 'messages', 'index');
        $acl->allow('Users', 'auth', 'logout');
        $acl->allow('Guests', 'firstentry', 'index');
        return $acl;
    }

    /**
     * This action is executed before execute any action in the application
     *
     * @apiDefine SecurityCheck
     * @apiParam {string} token токен для проверки/восстановления сессии пользователя
     */
    public function beforeDispatch(Event $event, Dispatcher $dispatcher)
    {

        $token = $this->request->has('token') && $this->request->get('token') ? $this->request->get('token') : "";
        $userId = $this->session->has('uid') ? $this->session->get('uid') : false;

        if ($token) {
            if ($userId) {
                $user_token_id = Tokens::tokenIsValid($token, $userId);
                if (!$user_token_id || $user_token_id != $userId) {
                    $this->session->set('uid', 0);
                    $this->session->destroy();
                }
            } else {
                //TODO потенциальная уязвимость, сделать генерацию токенов зависимой от чего либо, и проверять это "что либо"
                $user_token_id = Tokens::tokenIsValid($token);
                if ($user_token_id) {
                    $userId = $user_token_id;
                    $this->session->set('uid', $user_token_id);

                }
            }
        }

        //проверка прав на действие, допускаются только действия объявленные в ресурсах
        $controller = $dispatcher->getControllerName();

        $acl = $this->getAcl();
        if (!$acl->isResource($controller)) {
            $dispatcher->forward(array(
                'controller' => 'errors',
                'action' => 'index',
                'params' => array('error' => 'GENERAL.ERRORS.METHOD_NOT_FOUND')
            ));
            return false;
        }

        $allowed = false;
        $role = array('Guests');
        $action = $dispatcher->getActionName();
        if ($userId) {
            $role = Users::getRoles($userId);
        }
        if (!is_array($role)) {
            $allowed = $acl->isAllowed($role, $controller, $action);
        } else {
            $allowed = false;
            $i = 0;
            while (!$allowed && isset($role[$i])) {
                $allowed = $acl->isAllowed($role[$i], $controller, $action);
                $i++;
            }
        }
        if (!$allowed) {
            $dispatcher->forward(array(
                'controller' => 'errors',
                'action' => 'index',
                'params' => array('error' => 'GENERAL.ERRORS.ACCESS_DENIED')
            ));
            return false;
        }


    }
}