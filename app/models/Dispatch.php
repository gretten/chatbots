<?php

class Dispatch extends ModelBase
{

    public $id;
    public $uid;
    public $name;
    public $date;
    public $position;
    public $bot_vk_id;
    public $bot_fb_id;
    public $bot_telegram_id;
    public $bot_skype_id;
    public $bot_viber_id;
    public $filters;
    public $message_id;
    public $active;
    public $sending;

    //</editor-fold>

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSource(ucfirst($this->getSource()));
        $this->keepSnapshots(true);
    }


    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Dispatch[]|Dispatch
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }


    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Dispatch
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public static function checkUserPermission($uid, $campaingId, $action = "update")
    {

        $campaing = self::findFirst(array(
            'conditions' => 'uid = :uid: AND id = :id:',
            'bind' => array('uid' => $uid, 'id' => $campaingId)));
        if (!empty($campaing) && isset($campaing->id)) {

            return true;
        } else {
            return false;
        }
    }

    public static function getUserDispatches($uid)
    {
        $result_array = array();

        $campaingArray = Dispatch::find(array(
            'conditions' => 'uid = :uid:',
            'bind' => array('uid' => $uid)));

        if (!empty($campaingArray)) {

            foreach ($campaingArray as $r) {

                $result_array[$r->id] = $r->toArray();
            }

        }

        return $result_array;
    }

    public static function createUserDispatch($data_array)
    {

        $dispatch = new Dispatch();
        $success = $dispatch->create(
            [
                "id" => "",
                "uid" => $data_array['uid'],
                "name" => $data_array['name'],
                "date" => $data_array['date'],
                "position" => $data_array['position'],
                "bot_vk_id" => $data_array['bot_vk_id'],
                "bot_fb_id" => $data_array['bot_fb_id'],
                "bot_telegram_id" => $data_array['bot_telegram_id'],
                "bot_skype_id" => $data_array['bot_skype_id'],
                "bot_viber_id" => $data_array['bot_viber_id'],
                "filters" => $data_array['filters'],
                "message_id" => $data_array['message_id'],
                "active" => $data_array['active'],
                "sending" => $data_array['sending']
            ]
        );
        if ($success) {
            if ($dispatch->active) {
                MessageLine::createMessagesWhenDispatchActivated($dispatch);
            }
            return $dispatch;
        } else {
            return false;
        }
    }

    public static function updateUserDispatch($data_array)
    {

        $dispatch = Dispatch::findFirstById($data_array['id']);
        if (!$dispatch) {
            return array('error' => 'GENERAL.ERRORS.RECORD_NOT_FOUND');
        } else {
            if ($data_array['uid'] != $dispatch->uid) {
                return array('error' => 'GENERAL.ERRORS.ACCES_DENIED');
            }

        }
        $flagToActivate = false;
        $flagToDeactivate = false;
        if ($dispatch->acrive == 0 && $data_array['active'] == 1) {
            $flagToActivate = true;
        }
        if ($dispatch->acrive == 1 && $data_array['active'] == 0) {
            $flagToDeactivate = true;
        }

        $success = $dispatch->update(
            [
                "name" => $data_array['name'],
                "date" => $data_array['date'],
                "position" => $data_array['position'],
                "bot_vk_id" => $data_array['bot_vk_id'],
                "bot_fb_id" => $data_array['bot_fb_id'],
                "bot_telegram_id" => $data_array['bot_telegram_id'],
                "bot_skype_id" => $data_array['bot_skype_id'],
                "bot_viber_id" => $data_array['bot_viber_id'],
                "filters" => $data_array['filters'],
                "active" => $data_array['active']
            ]
        );
        if ($success) {
            if ($flagToActivate) {
                MessageLine::createMessagesWhenDispatchActivated($dispatch);
            }
            if ($flagToDeactivate) {
                MessageLine::deleteUnsentMessagesWhenDispatchDeactivated($dispatch);

            }
            return $dispatch;
        } else {
            return array('error' => 'GENERAL.ERRORS.CANT_UPDATE');
        }

    }

    public function activateDispatch()
    {
        if (!$this->active) {
            $this->active = 1;

            MessageLine::createMessagesWhenDispatchActivated($this);

            $this->update();
            return true;
        }
        return false;
    }

    public function deactivateDispatch()
    {
        if (!$this->active) {
            $this->active = 0;
            MessageLine::deleteUnsentMessagesWhenDispatchDeactivated($this);
            $this->update();
            return true;
        }
        return false;
    }


    public static function deleteRecord($id)
    {
        $dispatch = self::findFirst(array(
            'conditions' => 'id = :id:',
            'bind' => array('id' => $id)
        ));

        if (!$dispatch) {
            return array('error' => 'GENERAL.ERRORS.RECORD_NOT_FOUND');
        }

        if($dispatch->active){
            MessageLine::deleteUnsentMessagesWhenDispatchDeactivated($dispatch);
        }
        $result = $dispatch->delete();
        if ($result === true) {
            return array('result' => 'success');
        } else {
            return $result;
        }


    }


}
