<?php

class TunnelBlocks extends ModelBase
{

    public $id;
    public $campaign_id;
    public $name;
    public $delay_type;
    public $delay_time;
    public $cond_clicked;
    public $cond_bought;
    public $cond_ordered;
    public $cond_order_failed;

    //</editor-fold>

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSource("Tunnel_blocks");
    }


    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Tokens[]|Tokens
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }


    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Tokens
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }


    public static function getTunnelBlocks($campaign_id)
    {
        $result_array = array();


        $tunnelBlocksArray = TunnelBlocks::find(array(
            'conditions' => 'campaign_id = :campaign_id:',
            'bind' => array('campaign_id' => $campaign_id)));
        if (!empty($tunnelBlocksArray)) {

            foreach ($tunnelBlocksArray as $r) {
                $result_array[$r->id] = $r->toArray();
            }
            return $result_array;
        }
    }


}
