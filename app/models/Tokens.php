<?php

class Tokens extends ModelBase
{


    public $id;
    public $token;
    public $end_time;
    public $uid;

    //</editor-fold>

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSource(ucfirst($this->getSource()));
    }


    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Tokens[]|Tokens
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    public static function tokenIsValid($token, $uid = 0)
    {
        if ($uid) {
            $old_token = Tokens::findFirst(array(
                'conditions' => 'uid = :uid: AND token = :token:',
                'bind' => array('uid' => $uid, 'token' => $token)));
            if (!empty($old_token) && isset($old_token->uid) && $old_token->uid) {
                return $old_token->uid;
            } else {
                return false;
            }
        } else {
            $old_token = Tokens::findFirst(array(
                'conditions' => 'token = :token:',
                'bind' => array('token' => $token)));
            if (!empty($old_token) && isset($old_token->uid) && $old_token->uid) {
                return $old_token->uid;
            } else {
                return false;
            }
        }

    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Tokens
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /* TODO обновление времени токена*/
    public static function createNewTokenForUser($uid, $time = 0)
    {
        $one_day_in_seconds = 86400;
        if (!$time) $time = time() + $one_day_in_seconds;
        $token_text = md5($time + $uid + "some_random_nyan_cat_is_running_near");

        $old_token = Tokens::findFirst(array(
            'conditions' => 'uid = :uid:',
            'bind' => array('uid' => $uid)));
        if (!empty($old_token) && isset($old_token->uid) && $old_token->uid) {
            $old_token->end_date = $time;
            $old_token->token = $token_text;
            $success = $old_token->update();
            if ($success) {
                return $old_token;
            }
        } else {

            $token = new Tokens();
            $success = $token->create(
                [
                    "id" => "",
                    "token" => "$token_text",
                    "uid" => "$uid",
                    "end_time" => "$time",
                ]
            );
            if ($success) {
                return $token;
            }
        }
        return false;


    }


}
