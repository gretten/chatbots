<?php

class TunnelBlocksTree extends ModelBase
{


    public $tunnel_block_parent_id;
    public $tunnel_block_sibling_id;


    //</editor-fold>

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSource("Tunnel_blocks_tree");
    }


    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Tokens[]|Tokens
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }


    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Tokens
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public static function findTunnelBlocksParents($keysArray)
    {
        $result_array = array();
        $tunnelBlocksTreeArray = TunnelBlocksTree::find(
            [
                'tunnel_block_sibling_id IN ({tunnel_block_sibling_id:array})',
                'bind' => [
                    'tunnel_block_sibling_id' => $keysArray
                ]
            ]
        );
        if (!empty($tunnelBlocksTreeArray)) {

            foreach ($tunnelBlocksTreeArray as $r) {
                $result_array[$r->tunnel_block_parent_id][] = $r->tunnel_block_sibling_id;
            }

        }
        return $result_array;
    }


}
