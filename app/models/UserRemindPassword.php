<?php

class UserRemindPassword extends ModelBase
{


    public $id;
    public $uid;
    public $check_hash;
    public $expiration_date;


    //</editor-fold>

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSource('User_remind_password');
    }


    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return UserRemindPassword[]|UserRemindPassword
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return UserRemindPassword
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public static function createNewHashForUser($uid)
    {
        $one_day_in_seconds = 86400;


        $user = Users::findFirst(array(
            'conditions' => 'id = :id:',
            'bind' => array('id' => $uid)));
        if (!empty($user) && UserRemindPassword::deleteHashOfUser($uid)) {
            $remindPassword = new UserRemindPassword();
            $salt = 'Im_a_tonn_of_a_salt_to_be_decrypted_42!&*(@#Im_a_tonn_of_a_salt_to_be_decrypted_42!&*(@#Im_a_tonn_of_a_salt_to_be_decrypted_42!&*(@#';
            $mdHash = md5(time() + $salt + $uid);
            $success = $remindPassword->create(
                [
                    "id" => "",
                    "uid" => $uid,
                    "check_hash" => $mdHash,
                    "expiration_date" => time() + $one_day_in_seconds,
                ]
            );
            if ($success) {
                return $remindPassword;
            }
        }
        return false;


    }

    public static function deleteHashOfUser($uid)
    {
        try {

            UserRemindPassword::find(array('uid = :uid:', 'bind' => array('uid' => $uid)))->delete();

        } catch (Exception $e) {

            return false;
        }
        return true;
    }

    public static function deleteRecord($id)
    {
        $userRemindPassword = self::findFirst(array(
            'conditions' => 'id = :id:',
            'bind' => array('id' => $id)
        ));

        if (!$userRemindPassword) {
            return array('error' => 'GENERAL.ERRORS.RECORD_NOT_FOUND');
        }


        $result = $userRemindPassword->delete();
        if ($result === true) {
            return $result;
        } else {
            return $result;
        }


    }


}
