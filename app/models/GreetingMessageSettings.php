<?php

class GreetingMessageSettings extends ModelBase
{


    public $id;
    public $chat_bot_settings_id;
    public $message_id;
    public $bot_type_enum_id;


    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSource(ucfirst("Greeting_message_settings"));
    }


    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return GreetingMessageSettings[]|GreetingMessageSettings
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }


    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return GreetingMessageSettings
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }



    private static function isJson($string) {
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }



    public static function addGreetingMessage($bot_id, $uid, $bot_type_enum_id, $message_json)
    {
        //создаем сообщение
        if(!self::isJson($message_json))$message_json = json_encode($message_json);
        $message = new Message();
        $message->uid = $uid;
        $message->json = $message_json;
        $success = $message->create();
        if ($success) {
            // приписываем собщение к боту
            $greeting_message_record = new GreetingMessageSettings();
            $greeting_message_record->chat_bot_settings_id = $bot_id;
            $greeting_message_record->message_id = $message->id;
            $greeting_message_record->bot_type_enum_id = $bot_type_enum_id;
            $success = $greeting_message_record->create();
            if ($success) {

                return $message->id;
            }
        }

        return false;

    }

    public static function updateGreetingMessage($message_id, $message_json)
    {

        $message = Message::findFirstById($message_id);
        if ($message) {
            $message->json = $message_json;
            $success = $message->update();
            return $success;
        }
        return false;
    }

    public static function deleteGreetingMessage($message_id)
    {
        $message = Message::findFirstById($message_id);
        $greeting_message = GreetingMessageSettings::findFirst(array('message_id = :message_id:', 'bind' => array('message_id' => $message_id)));
        if ($message && $greeting_message) {

            $success = $message->delete();
            if ($success) {
                $success = $greeting_message->delete();
                if ($success) {
                    return true;
                }
            }


        }
        return false;
    }

    public static function deleteAllGreetingMessages($bot_id)
    {
        try {
            $greeting_messages = GreetingMessageSettings::find(array('id = :id:', 'bind' => array('id' => $bot_id)));
            foreach ($greeting_messages as $greeting_message) {
                $message = Message::findFirstById($greeting_message->message_id);
                $greeting_message->delete();
                $message->delete();
            }
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    public static function getBotGreetingsMessages($bot_id, $bot_enum_id, $uid)
    {
        $result = array();
        //получаем массив сообщений которые надо найти
        $greeting_messages_array = GreetingMessageSettings::find(array('chat_bot_settings_id = :chat_bot_settings_id:', 'bind' => array('chat_bot_settings_id' => $bot_id)));
        $keys_array = array_keys($greeting_messages_array);
        if (!empty($keys_array)) {
            switch ($bot_enum_id) {
                case VK_ID:
                    $messages_array = BotVkSettings::find(
                        [
                            'id IN ({id:array})',
                            'bind' => [
                                'id' => $keys_array
                            ]
                        ]
                    );
                    if (!empty($messages_array)) {
                        $result = $messages_array;
                    }
                    break;
                case FB_ID:
                    $messages_array = BotFbSettings::find(
                        [
                            'id IN ({id:array})',
                            'bind' => [
                                'id' => $keys_array
                            ]
                        ]
                    );
                    if (!empty($messages_array)) {
                        $result = $messages_array;
                    }
                    break;

                case VIBER_ID:
                    $messages_array = BotViberSettings::find(
                        [
                            'id IN ({id:array})',
                            'bind' => [
                                'id' => $keys_array
                            ]
                        ]
                    );
                    if (!empty($messages_array)) {
                        $result = $messages_array;
                    }
                    break;

                case SKYPE_ID:
                    $messages_array = BotSkypeSettings::find(
                        [
                            'id IN ({id:array})',
                            'bind' => [
                                'id' => $keys_array
                            ]
                        ]
                    );
                    if (!empty($messages_array)) {
                        $result = $messages_array;
                    }
                    break;

                case TELEGRAM_ID:
                    $messages_array = BotTelegramSettings::find(
                        [
                            'id IN ({id:array})',
                            'bind' => [
                                'id' => $keys_array
                            ]
                        ]
                    );
                    if (!empty($messages_array)) {
                        $result = $messages_array;
                    }
                    break;


            }
        }
        return $result;
    }
}
