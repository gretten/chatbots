<?php

class ChatUserSubscriptions extends ModelBase
{

    public $id;
    public $chat_user_id;
    public $chat_bot_settings_id;
    public $bot_type_enum_id;
    public $dispatch_id = 0;
    public $subscribed;

    //</editor-fold>

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSource("Chat_user_subscriptions");
    }


    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return ChatUserSubscriptions[]|ChatUserSubscriptions
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }


    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return ChatUserSubscriptions
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }


}
