<?php

class Users extends ModelBase
{


    public $id;
    public $name;
    public $lastname;
    public $email;
    public $pass;
    public $created;
    public $last_seen;
    public $phone;
    public $country;
    public $city;
    public $role_id;
    public $main_user_id;
    public $end_date;


    //</editor-fold>

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSource(ucfirst($this->getSource()));
    }

    public static function encryptUserPassword($pass)
    {
        $salt = 'Im_a_tonn_of_a_salt_to_be_decrypted_42!&*(@#Im_a_tonn_of_a_salt_to_be_decrypted_42!&*(@#Im_a_tonn_of_a_salt_to_be_decrypted_42!&*(@#';
        $result = crypt($pass, $salt);
        return $result;
    }


    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Users[]|Users
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Users
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }


    public static function getRoles($uid)
    {
        return array('Guests');
    }

    public static function updateUserPassword($data_array)
    {

        $user = Users::findFirstById($data_array['id']);
        $user->pass = $data_array['password'];
        $result = $user->update();
        //удаляем использованную запись, если все прошло успешно
        if ($result) {
            UserRemindPassword::deleteHashOfUser($data_array['id']);
            return $user;
        }else{
            return false;
        }
    }
    /**
     * Возвращает роли пользователя с указанным идентификатором
     * @param $id - идентификатор пользователя
     * @return array
     */
    /*public static function getRoles($id) {
        if (!$id) {
            return array('Guests');
        } else {
            $roles = UsersRoles::find(array(
                'conditions' => 'user_id = :id:',
                'bind' => array('id' => $id)
            ));
            if (!empty($roles)) {
                $result = array();
                foreach ($roles as $r) {
                    $result[] = Roles::findFirst($r->role_id)->name;
                }
                return $result;
            } else {
                return array('Users');
            }
        }
    }*/

}
