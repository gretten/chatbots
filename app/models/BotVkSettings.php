<?php

class BotVkSettings extends ModelBase
{


    public $id;
    public $uid;
    public $token;
    public $registered;
    public $name;
    public $group_id;
    public $hash_id='';
    //</editor-fold>

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSource('Bot_vk_settings');
    }


    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return BotVkSettings[]|BotVkSettings
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return BotVkSettings
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }


}
