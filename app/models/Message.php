<?php

class Message extends ModelBase
{


    public $id;
    public $uid;
    public $json;


    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSource(ucfirst($this->getSource()));
    }


    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Message[]|Message
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }


    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Message
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }


    public static function checkMessageJson($json, $system = "")
    {
        return true;
    }


}
