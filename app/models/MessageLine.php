<?php

class MessageLine extends ModelBase
{

    public $id;
    public $message_id;
    public $date_to_send;
    public $status;
    public $chat_user_id;
    public $bot_type_enum_id;
    public $chat_bot_settings_id;
    public $dispatch_id;

    //</editor-fold>

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSource("Message_line");
    }

    public static function createMessagesWhenDispatchActivated($dispatch_object)
    {

        if ($dispatch_object->bot_vk_id) {
            self::_createMessageLine($dispatch_object->message_id, $dispatch_object->date, VK_ID, $dispatch_object->bot_vk_id, $dispatch_object->id);
        }
        if ($dispatch_object->bot_fb_id) {
            self::_createMessageLine($dispatch_object->message_id, $dispatch_object->date, FB_ID, $dispatch_object->bot_fb_id, $dispatch_object->id);
        }
        if ($dispatch_object->bot_telegram_id) {
            self::_createMessageLine($dispatch_object->message_id, $dispatch_object->date, TELEGRAM_ID, $dispatch_object->bot_telegram_id, $dispatch_object->id);
        }
        if ($dispatch_object->bot_skype_id) {
            self::_createMessageLine($dispatch_object->message_id, $dispatch_object->date, SKYPE_ID, $dispatch_object->bot_skype_id, $dispatch_object->id);
        }
        if ($dispatch_object->bot_viber_id) {
            self::_createMessageLine($dispatch_object->message_id, $dispatch_object->date, VIBER_ID, $dispatch_object->bot_viber_id, $dispatch_object->id);
        }
    }

    public static function deleteUnsentMessagesWhenDispatchDeactivated($dispatch_object)
    {

        MessageLine::find([
            "dispatch_id = :dispatch_id: AND status = :status:",
            "bind" => [
                "dispatch_id" => $dispatch_object->id,
                "status" => 0,
            ],
        ])->delete();
    }

    private static function _createMessageLine($message_id, $date_to_send, $bot_type_enum, $chat_bot_settings_id, $dispatch_id)
    {

        //получаем список ид пользователей привязанных к боту


        $result_array = ChatUserSubscriptions::find([
            "bot_type_enum_id = :bot_type_enum_id: AND chat_bot_settings_id = :chat_bot_settings_id:",
            "bind" => [
                "bot_type_enum_id" => $bot_type_enum,
                "chat_bot_settings_id" => $chat_bot_settings_id,
            ],
        ]);

        $sql = new sqlHelper();
        $sql_query = array();
        //создаем для каждого свою запись
        if (count($result_array)) {
            foreach ($result_array as $item) {
                $sql_query[] = '("' . $message_id . '", "' . $date_to_send . '", 0, "' . $item->chat_user_id . '", "' . $bot_type_enum . '", "' . $chat_bot_settings_id . '", "' . $dispatch_id . '")';
            }
        }

        $sql_query = 'INSERT INTO `chatbots`.`Message_line` (`message_id`, `date_to_send`, `status`, `chat_user_id`, `bot_type_enum_id`, `chat_bot_settings_id`, `dispatch_id`) VALUES ' . implode(',', $sql_query);

        $sql::executeSql($sql_query);
    }

    private static function _deleteUnsentMessageLine($message_id, $date_to_send, $bot_type_enum, $chat_bot_settings_id)
    {


    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return MessageLine[]|MessageLine
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }


    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return MessageLine
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }


}
