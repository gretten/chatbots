<?php

class BotSkypeSettings extends ModelBase
{


    public $id;
    public $uid;
    public $name;
    public $app_name;
    public $app_id;
    public $app_pass;
    public $message_endpoint;
    public $registered;
    public $hash_id='';

    //</editor-fold>

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSource('Bot_skype_settings');
    }


    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return BotSkypeSettings[]|BotSkypeSettings
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return BotSkypeSettings
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }


}
