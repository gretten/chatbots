<?php

class ChatUser extends ModelBase
{

    public $id;
    public $bot_type_enum_id;
    public $system_id;
    public $name;
    public $phone;
    public $email;
    public $gender;
    public $locale;
    public $comments;
    //</editor-fold>

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSource("Chat_user");
    }


    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return ChatUser[]|ChatUser
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }


    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return ChatUser
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }


}
